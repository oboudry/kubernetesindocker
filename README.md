# Kubernetes in Docker

Create a Centos/7 virtual machine with a Kubernetes in Docker 5 nodes cluster.

## Getting Started

These instructions will get you up and running on your local machine for
development and testing purposes.

### Prerequisites

Install Virtualbox https://www.virtualbox.org/ 

Install Vagrant https://www.vagrantup.com/

Install git https://gitforwindows.org/

Clone the repository onto your local machine

```
git clone https://oboudry@bitbucket.org/oboudry/kubernetesindocker.git
cd kubernetesindocker
```

Provision the virtual machine

```
vagrant up
```

Logon to the VM using `vagrant ssh` and use `kubectl` to administer your cluster.

```
kubectl get nodes
```

Once you're finished playing with the VM, you can either suspend it (can be restarted with `vagrant up`) or destroy it.

```
vagrant suspend
vagrant destroy
```

## Authors

* **Olivier Boudry** - *Vagrantfile and config.yaml*

## License

Copyright 2019 Olivier Boudry

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
