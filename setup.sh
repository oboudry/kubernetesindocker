#!/bin/bash
dnf -y update
dnf install -y device-mapper-persistent-data lvm2

# Install Docker
dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
dnf install docker-ce --nobest -y
usermod -aG docker vagrant
mkdir /etc/docker
mv /home/vagrant/daemon.json /etc/docker/
systemctl enable docker
systemctl start docker

# Install Go
curl -L -O https://golang.org/dl/go1.16.2.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.16.2.linux-amd64.tar.gz
rm go1.16.2.linux-amd64.tar.gz -f

# Install Kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl

sudo -i -u vagrant << EOF
  echo 'export PATH=\$PATH:/usr/local/go/bin:/home/vagrant/go/bin' >> ~/.bash_profile
  source ~/.bash_profile
  GO111MODULE="on" go get sigs.k8s.io/kind@v0.10.0 && kind create cluster --config=./config.yaml
EOF
